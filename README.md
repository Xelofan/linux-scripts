# linux-scripts

### Shell Setup
`
curl https://gitlab.com/Xelofan/linux-scripts/-/raw/main/shell-setup.sh | bash
`

### ~~Docker (+compose) Autoinstall (CentOS 8 based)~~
`
curl https://gitlab.com/Xelofan/linux-scripts/-/raw/main/centos8-docker-installer.sh | bash
`
### ~~Docker (+compose) Autoinstall (Debian 10)~~
`
curl https://gitlab.com/Xelofan/linux-scripts/-/raw/main/debian10-docker-installer.sh | bash
`
