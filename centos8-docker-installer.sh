#!/bin/bash
# Automatic Docker installer for CentOS 8 based distros (Alma/RockyLinux 8, CentOS 8, etc.)

sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io -y
sudo systemctl enable --now docker

sudo usermod -aG docker ${USER}

# + Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/v2.5.1/docker-compose-linux-$(uname -p)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
