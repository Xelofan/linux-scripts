#!/bin/bash
# A script to automatically install git, zsh, ohmyzsh, omz config, omz plugins and apply some DNF configs

# Install git, zsh, util-linux-user(for chsh), neofetch
sudo dnf in git zsh util-linux-user neofetch -y
echo -e "\n-> Installed git, zsh, util-linux-user, neofetch <-\n"

# Change current user's shell to zsh
#sudo chsh -s /bin/zsh $USER
#echo -e "\n-> Changed shell to ZSH <-\n"

# Install ohmyzsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Install my omz plugins
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
echo -e "\n-> Installed zsh-autosuggestions <-\n"

# Clone & apply my dotfiles
git clone https://gitlab.com/Xelofan/dotfiles ~/dotfiles
cp ~/dotfiles/zshrc ~/.zshrc
echo -e "\n-> Cloned & applied Xelofan's dotfiles"

echo -e "\n-> Setup has been hopefully successful,\nplease restart your shell! <-"